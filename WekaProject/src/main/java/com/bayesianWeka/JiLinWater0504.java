package com.bayesianWeka;

import weka.classifiers.AbstractClassifier;
import weka.core.Instance;
import weka.core.Instances;


/**
 * @Author: Decade
 * @Date: 2021/5/4 9:28
 */
public class JiLinWater0504 extends AbstractClassifier {

//    private int m_NumAttributes;
//
//    private List<Integer>[] lists;

    private CommonUtils commonUtils;

    @Override
    public void buildClassifier(Instances instances) throws Exception {
        commonUtils = new CommonUtils(instances);

        commonUtils.Count3XAnd4X();

        //计算H(x|x)  H(x|xx) 和 H(x|xxx)
        double[][] xxEntropyInf = commonUtils.getXXEntropyInf();
        double[][][] xxxEntropyInf = commonUtils.getXXXEntropyInf();
        double[][][][] xxxxEntropyInf = commonUtils.getXXXXEntropyInf();

//        System.out.println("xxEntropyInf[0][5] = " + xxEntropyInf[0][5]);
//        System.out.println("xxxEntropyInf[0][3][5] = " + xxxEntropyInf[0][3][5]);
//        System.out.println("xxxxEntropyInf[6][4][7][8] = " + xxxxEntropyInf[6][4][7][8]);
//        System.out.println("xxxEntropyInf[6][7][8] = " + xxxEntropyInf[6][7][8]);
//        System.out.println("xxxEntropyInf[6][4][7] = " + xxxEntropyInf[6][4][7]);
//        System.out.println("xxxEntropyInf[6][4][8] = " + xxxEntropyInf[6][4][8]);


//        System.out.println("xxxEntropyInf[9][2][6] = " + xxxEntropyInf[9][2][6]);
//        System.out.println("xxxxEntropyInf[9][2][6][4] = " + xxxxEntropyInf[9][2][6][4]);
//        System.out.println("xxxxEntropyInf[9][2][6][7] = " + xxxxEntropyInf[9][2][6][7]);
//        System.out.println("xxxxEntropyInf[9][2][6][8] = " + xxxxEntropyInf[9][2][6][8]);

//        System.out.println("xxxxEntropyInf[1][0][9] = " + xxxEntropyInf[1][0][9]);
//        for (int i = 2; i < 9; i++) {
//            System.out.println("xxxxEntropyInf[1][0][9][" + i + "] = " + xxxxEntropyInf[1][0][9][i]);
//        }

    }

    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {

        double jointP;

        jointP = commonUtils.P(instance,3) * commonUtils.P(instance,4) * commonUtils.P(instance,7) * commonUtils.P(instance,8)
                 * commonUtils.P_xx(instance,5,3) * commonUtils.P_xxx(instance,0,5,3) * commonUtils.P_xxxx(instance,9,6,2,7)
                 * commonUtils.P_xxxx(instance,1,0,9,5) * commonUtils.P_xxxx(instance,6,4,7,8);

        double logLikeliHood = Math.log(jointP) / Math.log(2);
        System.out.println(logLikeliHood);

        return new double[]{1.0,2.0};
    }

    public static void main(String[] args) {
        runClassifier(new JiLinWater0504(),args);
    }
}
