package com.bayesianWeka;

import weka.classifiers.AbstractClassifier;
import weka.core.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @Author: Decade
 * @Date: 2021/3/5 15:53
 *
 * K2DB贝叶斯网络分类器
 *
 */
public class KDB extends AbstractClassifier {

    /*类标签的个数*/
    private int m_NumClasses;

    /*属性的个数，在weka中，本值为预测属性和类属性之和*/
    private int m_NumAttributes;

    /*训练集类标签的索引*/
    private int m_ClassIndex;

    /** KDB模型的父结点 下标一代表不同的属性 下标二只有0和1 代表两个父结点的编号*/
    private int[][] m_parents;

    /*通用工具类*/
    private CommonUtils commonUtils;

    /**
     * 训练阶段，本阶段主要做两件事情：1. 变量的初始化 数组空间的申请 2. General版本模型分类器的构建
     * @param instances 测试集对象
     */
    @Override
    public void buildClassifier(Instances instances) throws Exception {

        commonUtils = new CommonUtils(instances);
        commonUtils.CalculateCountXxxy();
//        System.out.println("hello world");

//        System.out.println("FXXXY计算完毕");

        m_ClassIndex = commonUtils.getM_ClassIndex();          //类标签的索引
        m_NumAttributes = commonUtils.getM_NumAttributes();    //属性个数 算上类标签的个数
        m_NumClasses = commonUtils.getM_NumClasses();          //类标签的个数
        m_parents = new int[m_NumAttributes][2];

        /** KDB的模型构建过程 */
        //有几个点肯定是固定的，第0、1、2这三个点的父结点是唯一确定的，所以它们仨可以直接初始化
        //从第4个点开始，找出互信息里最大的前两个，然后给它建立连接就完事儿了 简化问题
        /* 计算互信息，并根据互信息的值对属性进行排序 */
        double[] MiXy = commonUtils.getMutualInfor();   //互信息
        Point[] points = new Point[m_NumAttributes-1];
        for (int i = 0; i < m_NumAttributes-1; i++) {
            //由于那个m_classIndex一直都是最后一个属性，在这里我就直接把最后一个给跳过了，否则没办法调用sort方法
            //如果要上传到weka库的话，那就把m_classIndex加上，后面多加一个判断
            points[i] = new Point(i,MiXy[i]);
        }
        Arrays.sort(points);
//        System.out.println(Arrays.toString(points));

        for (int i = 0; i < m_NumAttributes; i++) {
            Arrays.fill(m_parents[i],-1);       //先初始化为-1
        }
        if (m_NumAttributes - 1 < 3){
            //如果属性的值个数小于3个时，只需要给排第二位的属性设置父结点即可
            m_parents[points[1].getPointNum()][0] = points[0].getPointNum();
        } else {
            //正常情况下基本都是大于等于3个属性值的
            m_parents[points[1].getPointNum()][0] = points[0].getPointNum();    //第一个节点第一个父结点为第0个节点
            m_parents[points[2].getPointNum()][0] = points[0].getPointNum();    //第二个节点第一个父结点为第0个节点
            m_parents[points[2].getPointNum()][1] = points[1].getPointNum();    //第二个节点第二个父结点为第1个节点
        }

        ArrayList<Integer> available = new ArrayList<>();
        ArrayList<Integer> inMap = new ArrayList<>();          //来两个集合，用来存放入图的结点和未入图的结点

        for (int i = 0; i < m_NumAttributes-1; i++) {
            if (m_NumAttributes - 1 <= 3)    //如果属性个数小于等于3  那就已经完成模型构建了 不用进行下一步了
                break;
            if (i < 3)
                inMap.add(points[i].getPointNum());
            else
                available.add(points[i].getPointNum());
        }
//        available.forEach(System.out::println);
//        System.out.println("***");
//        inMap.forEach(System.out::println);

        double[][] CmiXxy = commonUtils.getConditionMutualInfor(); //条件互信息

        //接下来就是从数组中找两个最大的
        Iterator<Integer> itAvailable = available.iterator();
        while (!available.isEmpty()){
            //取available集合中的第一个
            int availablePoint = itAvailable.next();
            //寻找这个点的两个父结点
            double maxCmi = -Double.MAX_VALUE;
            double secondCmi = -Double.MAX_VALUE;
            int firstParent = -1;
            int secondParent = -1;
            for (int itInmap : inMap){
                //遍历inmap中的每一个元素
                if (CmiXxy[availablePoint][itInmap] > maxCmi){
                    secondCmi = maxCmi;
                    maxCmi = CmiXxy[availablePoint][itInmap];
                    firstParent = itInmap;
                }
                else if (CmiXxy[availablePoint][itInmap] > secondCmi && CmiXxy[availablePoint][itInmap] <= maxCmi){
                    secondCmi = CmiXxy[availablePoint][itInmap];
                }
            }
            for (int itInmap : inMap){
                if (CmiXxy[availablePoint][itInmap] == secondCmi)
                    secondParent = itInmap;
            }
            m_parents[availablePoint][0] = firstParent;
            m_parents[availablePoint][1] = secondParent;
//            available.remove(availablePoint);
            itAvailable.remove();
            inMap.add(availablePoint);
        }
//        for (int i = 0; i < m_NumAttributes - 1; i++) {
//            System.out.println(Arrays.toString(m_parents[i]));
//        }
    }

    /**
     * 分类/决策阶段，本阶段主要做两件事：
     * 1. 给定测试样本，计算联合概率，并返回归一化后的联合概率(即后验概率)，用于分类器做决策
     * 2. Local版本分类器的构建
     * @param instance 一个测试样本实例
     * @return 后验概率
     */
    public double[] distributionForInstance(Instance instance) throws Exception {

        double[] probs = new double[m_NumClasses];
        for (int i = 0; i < m_NumClasses; i++) {
            probs[i] = commonUtils.P(i);        //P(y)
        }

        for (int x1 = 0; x1 < m_NumAttributes; x1++) {
            if(x1 == m_ClassIndex)
                continue;
            int parent1 = m_parents[x1][0];              //父结点1的序号
            int parent2 = m_parents[x1][1];              //父结点2的序号

            for (int y = 0; y < m_NumClasses; y++) {
                if (parent1 == -1){
                    //说明是互信息最大的那个点，联合概率乘P(x1|y)
                    probs[y] *= commonUtils.P(instance,x1,y);
                }
                else if (parent2 == -1){
                    //parent1不为-1   parent2为-1 说明是互信息第二大的那个点，联合概率乘P(x1|parent1,y)
                    //P(X0=x0|X1=x1,Y=y) = (F(x0,x1,y) + 1/F(x0)) / (1+F(x1,y))
                    probs[y] *= commonUtils.P(instance,x1,parent1,y);
                }
                else {
                    //parent1和2均不为-1 为常规点 联合概率乘P(x1|parent1,parent2,y)
                    probs[y] *= commonUtils.P(instance,x1,parent1,parent2,y);
//                    commonUtils.displayCount();
                }
            }
        }

        Utils.normalize(probs);

        return probs;
    }

    /**
     * toString方法，本方法可以显示在weka的输出结果当中，可以帮助我们判断拿到的结果是出自哪个分类器
     * @return 描述本方法的字符串
     */
    @Override
    public String toString() {
        return "KDB";
    }

    /**
     * 主方法 用于运行分类器 runClassifier(new KDB(), args);语句中的KDB()要与分类器名字(即本java文件的主类名字)一一对应
     * @param args 命令行参数
     */
    public static void main(String[] args) {
        runClassifier(new KDB(), args);
    }
}

/*
* Point类实现了Comparable接口 实现了在KDB中的先把所有预测属性X与Y的互信息排序的功能
* */
class Point implements Comparable<Point>{
    private final int pointNum;
    private final double pointMi;

    /**
     * 构造方法 初始化类中的属性
     * @param pointNum 属性的编号
     * @param pointMi 该属性与类属性Y的互信息值
     */
    public Point(int pointNum, double pointMi) {
        this.pointNum = pointNum;
        this.pointMi = pointMi;
    }

    /**
     * 获取属性的编号
     * @return 属性的编号
     */
    public int getPointNum() {
        return pointNum;
    }

    /**
     * 获取I(X;Y)
     * @return I(X;Y)
     */
    public double getPointMi() {
        return pointMi;
    }

    /**
     * 实现了Comparable接口的方法 用于排序
     * @param point 属性的实例
     * @return 0/1/-1
     */
    @Override
    public int compareTo(Point point) {
        return -Double.compare(this.pointMi, point.pointMi);
    }

    /**
     * 获取point对象的具体信息
     * @return point对象的具体信息
     */
    @Override
    public String toString() {
        return "Point{" +
                "pointNum=" + pointNum +
                ", pointMi=" + pointMi +
                '}';
    }
}
