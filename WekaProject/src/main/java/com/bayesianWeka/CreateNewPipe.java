package com.bayesianWeka;

/**
 * @Author: Decade
 * @Date: 2021/4/17 18:03
 * 用于生成批处理命令的脚本
 */
public class CreateNewPipe {
    public static void main(String[] args) {
        String[] dataSetName = new String[]{
                "covtype",
                "donation",
                "Diabetic-RD",
                "Website-Phishing",
                "Polish-companies1",
                "Polish-companies2",
                "Polish-companies3",
                "Polish-companies4",
                "Polish-companies5",
                "seismic-bumps",
                "Bach-Choral-Harmony",
                "musk2",
                "Electrical-Grid",
                "Firm-Teacher",
                "EEG-Eye-State",
                "Occupancy",
                "Avila",
                "Bank Marketing",
                "Activity-recognition-with",
                "Skin-Segmentation"
        };

        String[] dataSetName1 = new String[]{"contact-lenses",
                "lung-cancer",
                "labor",
                "labor-negotiations",
                "post-operative",
                "zoo",
                "promoters",
                "echocardiogram",
                "lymphography",
                "iris",
                "teaching-ae",
                "hepatitis",
                "wine",
                "autos",
                "sonar",
                "glass-id",
                "new-thyroid",
                "audio",
                "heart",
                "hungarian",
                "heart-disease-c",
                "soybean-large",
                "primary-tumor",
                "bupa",
                "ionosphere",
                "dermatology",
                "horse-colic",
                "house-votes-84",
                "musk1",
                "cylinder-bands",
                "chess",
                "syncon",
                "balance-scale",
                "soybean",
                "credit-a",
                "crx",
                "breast-cancer-w",
                "pima-ind-diabetes",
                "vehicle",
                "anneal",
                "tic-tac-toe",
                "vowel",
                "german",
                "led",
                "contraceptive-mc",
                "yeast",
                "volcanoes",
                "car",
                "mfeat-mor",
                "segment",
                "hypothyroid",
                "splice-c4.5",
                "kr-vs-kp",
                "dis",
                "hypo",
                "sick",
                "abalone",
                "spambase",
                "waveform-5000",
                "phoneme",
                "wall-following",
                "page-blocks",
                "optdigits",
                "satellite",
                "mushrooms",
                "thyroid",
                "pendigits",
                "sign",
                "nursery",
                "seer_mdl",
                "magic",
                "letter-recog",
                "adult",
                "shuttle",
                "connect-4",
                "waveform",
                "localization",
                "census-income",
                "covtype",
                "poker-hand",
                "donation",
                "Diabetic-RD",
                "Website-Phishing",
                "Polish-companies1 ",
                "Polish-companies2",
                "Polish-companies3",
                "Polish-companies4",
                "Polish-companies5",
                "seismic-bumps",
                "Bach-Choral-Harmony",
                "musk2",
                "Electrical-Grid",
                "Firm-Teacher",
                "EEG-Eye-State",
                "Occupancy",
                "Avila ",
                "bank-marketing",
                "Activity-recognition-with",
                "Skin-Segmentation"
        };

        int option = 4;     //如果处理缺失值那就是1  离散化那就是2

        if (option == 1){
            for (int i = 0; i < dataSetName.length; i++) {
                System.out.println("java weka.filters.unsupervised.attribute.ReplaceMissingValues -i E:\\OldDataSet\\" + dataSetName[i] +
                        ".arff -o E:\\OldDataSet1\\"+ dataSetName[i] +".arff -c last");
            }
        }
         else if (option == 2){
            for (int i = 0; i < dataSetName.length; i++) {
                System.out.println("java weka.filters.supervised.attribute.Discretize -i E:\\OldDataSet1\\" + dataSetName[i] +
                        ".arff -o E:\\DataSetForWeka\\"+ dataSetName[i] +".arff -c last");
            }
        }
         else if (option == 3){
            for (int i = 0; i < dataSetName1.length; i++) {
                System.out.println("java com.bayesianWeka.CFWNB -t F:\\DataSetForWeka\\"+dataSetName1[i]+".arff  -x 10 >>" +
                        "E:\\petalResult\\CFWNB_01_Time\\"+dataSetName1[i]+".txt");
            }
        }
         else if (option == 4){
            for (int i = 0; i < dataSetName1.length; i++) {
                System.out.println("");
            }
        }
    }
}
